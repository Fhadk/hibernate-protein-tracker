package com.hibernate.test.app;

public class ProtienData {
	private int protienTotal;
	private int goal;
	private User user;
	private int ProtienID;
	
	public ProtienData() {}
	
	
	/**
	 * @return the protienTotal
	 */
	public int getProtienTotal() {
		return protienTotal;
	}
	/**
	 * @param protienTotal the protienTotal to set
	 */
	public void setProtienTotal(int protienTotal) {
		this.protienTotal = protienTotal;
	}
	/**
	 * @return the goal
	 */
	public int getGoal() {
		return goal;
	}
	/**
	 * @param goal the goal to set
	 */
	public void setGoal(int goal) {
		this.goal = goal;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public int getProtienID() {
		return ProtienID;
	}


	public void setProtienID(int protienID) {
		ProtienID = protienID;
	}
	

}
