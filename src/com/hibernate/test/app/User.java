/**
 * 
 */
package com.hibernate.test.app;

import java.util.ArrayList;
import java.util.List;

/**
 * @author FahadKh1
 *
 */
public class User {
	
	private int ID;
	private String name;
//	private int protienTotal;
//	private int goal;
//	private Map<String, UserHistory> history = new HashMap<String, UserHistory>(); 	// For 'SET' set<type>=  new hashset<type> || FOR 'LIST' change 'Set' to 'List' and new ArraryList    ||   For 'MAP' Map<String,<type>> =  new hashmap<String,<type>>
	private ProtienData protienData;
	private List<UserHistory> history =  new ArrayList<UserHistory>();
	
	User(){
		setProtienData(new ProtienData());
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return ID;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int ID) {
		this.ID = ID;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the history
	 */
	public List<UserHistory> getHistory() {
		return history;
	}

	/**
	 * @param history the history to set
	 */
	public void setHistory(List<UserHistory> history) {
		this.history = history;
	}


//	public ProtienData getProtienData() {
//		return protienData;
//	}
//
//
//	public void setProtienData(ProtienData protienData) {
//		this.protienData = protienData;
//	}


//	public int getProtienTotal() {
//		return protienTotal;
//	}
//
//
//	public void setProtienTotal(int protienTotal) {
//		this.protienTotal = protienTotal;
//	}
//
//
//	public int getGoal() {
//		return goal;
//	}
//
//
//	public void setGoal(int goal) {
//		this.goal = goal;
//	}
	
	public void addHistory(UserHistory historyItem){
		historyItem.setUser(this);
		history.add(historyItem);
	}
	public ProtienData getProtienData() {
		return protienData;
	}
	public void setProtienData(ProtienData protienData) {
		this.protienData = protienData;
		protienData.setUser(this);
	}

}
