package com.hibernate.test.app;

import java.util.Date;

public class UserHistory {
	private Date entryTime;
	private String entry;
	private int historyID;
	private User user;
	
	
	public UserHistory(Date entryTime,String entry) {
		super();
		this.entry = entry;
		this.entryTime = entryTime;
	}
	
	public UserHistory() {}
	
//	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((entry == null) ? 0 : entry.hashCode());
//		result = prime * result + ((entryTime == null) ? 0 : entryTime.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		UserHistory other = (UserHistory) obj;
//		if (entry == null) {
//			if (other.entry != null)
//				return false;
//		} else if (!entry.equals(other.entry))
//			return false;
//		if (entryTime == null) {
//			if (other.entryTime != null)
//				return false;
//		} else if (!entryTime.equals(other.entryTime))
//			return false;
//		return true;
//	}

	/**
	 * @return the entry
	 */
	public String getEntry() {
		return entry;
	}
	
	
	/**
	 * @param entry the entry to set
	 */
	public void setEntry(String entry) {
		this.entry = entry;
	}
	/**
	 * @return the entryTime
	 */
	public Date getEntryTime() {
		return entryTime;
	}
	/**
	 * @param entryTime the entryTime to set
	 */
	public void setEntryTime(Date entryTime) {
		this.entryTime = entryTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getHistoryID() {
		return historyID;
	}

	public void setHistoryID(int historyID) {
		this.historyID = historyID;
	}
}
