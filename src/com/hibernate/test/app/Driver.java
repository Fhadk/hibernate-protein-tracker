/**
 * 
 */
package com.hibernate.test.app;

import java.util.Date;

import org.hibernate.Session;

/**
 * @author FahadKh1
 *
 */
public class Driver {

	public Driver() {}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Hello World !! ");
		Session session = HibernateUtilities.getSessionFactory().openSession();
		User user = new User();
		
		try{
			// Pushing Data in DB
			session.beginTransaction();
			
			
			user.setName("Fahad");
	//		user.getHistory().put("GC2223",new UserHistory(new Date(), "Setting Name !! " ));   			for HashMap
			user.addHistory(new UserHistory(new Date(), "Setting Name !!"));
			user.getProtienData().setGoal(250);
			user.getProtienData().setProtienTotal(100);
	//		user.getHistory().put("GC2224",new UserHistory(new Date(), "Setting Goal to 250 !! " ));		for HashMap
			user.addHistory(new UserHistory(new Date(),"Setting Goal 250 !!"));
			
			session.save(user);
			session.getTransaction().commit();
		}catch(Exception e){
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			System.out.println(e.toString());
		}
		
		// Pulling Data from DB
		session.beginTransaction();
		
		User loadedUser =  (User) session.get(User.class, 1);  // session.load 
		System.out.println("Name: " + loadedUser.getName());
		System.out.println("Goal : " +loadedUser.getProtienData().getGoal());
		System.out.println("ProtienTotal : " +loadedUser.getProtienData().getProtienTotal());
		
		
//		for(Entry<String, UserHistory> history: loadedUser.getHistory().entrySet()){
//			System.out.println( history.getValue().getEntryTime().toString()+" "+ history.getValue().getEntry().toString()+" "+history.getKey());			for HashMap
//		}
		
		for(UserHistory history:loadedUser.getHistory()){
			System.out.println(history.getEntryTime().toString()+" "+ history.getEntry().toString());			
		}
		
		loadedUser.setName("Khan");  // auto Update
		user.addHistory(new UserHistory(new Date(), "Changing Name !! " ));
		loadedUser.getProtienData().setProtienTotal(loadedUser.getProtienData().getProtienTotal() + 50);
		user.addHistory(new UserHistory(new Date(), "Adding Protien 50 !! " ));
		
		session.getTransaction().commit();
		session.close();
		HibernateUtilities.getSessionFactory().close();
	}
}
